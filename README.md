# k3s tests

This project aims to get a quick and non-secure way to create VM within Virtualbox with Vagrant to start playing with K3S.

## HOW-TO

We wille create 2 virtual machines:

- A single Control plane: `k3scp1`
- Two worker nodes: `k3sw01`

1. [Install Virtualbox on your machine](https://www.virtualbox.org/wiki/Downloads)
2. [Install Vagrant on your machine](https://developer.hashicorp.com/vagrant/tutorials/getting-started/getting-started-install)
3. If you're working on WSL2 (Windows + Ubuntu), you have to install Virtualbox on Windows and Vagrant on Linux. See [here](https://thedatabaseme.de/2022/02/20/vagrant-up-running-vagrant-under-wsl2/) for more details.
4. Update `Vagrantfile` replacing specific path on line 6.
5. ```vagrant up```
